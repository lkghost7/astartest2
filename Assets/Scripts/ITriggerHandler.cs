﻿ public interface ITriggerHandler {
        
        void Activate(bool isActive);
        
        bool IsProtectedFromEnvironment {get; set;}
        
    }

