﻿using System;
using UnityEngine;

public enum StateEnemys{
    movingLeft,
    movingRight,
    movingUp,
    movingDown
}

public class EnemysController : MonoBehaviour{
    [SerializeField] private GameObject playerGameObject;
    [SerializeField] private Transform  graphics;
    

    private StateEnemys currientStateEnemy;
   private Vector2 _ladderPos;

    private       Rigidbody2D rb;
    private const float       SPEED            = 2f;
    private       int         currentDirection = 1;

    public bool isPossableLadderMove {get; set;}

    // private bool _isLeft;
    // private bool _isRight;
    // private bool _isUp;
    // private bool _isDown;

    private bool _moveX = true;
    private bool _moveY;


    void Start() {
        rb = GetComponent<Rigidbody2D>();
        // _ladders = FindObjectOfType<Ladders>();
    }

    void Update() {
        
        
        Debug.Log("чему  равна переменная для лестницы  = " + isPossableLadderMove);
        // if(isPossableLadderMove) {
        //     rb.gravityScale = 0f;
        //     rb.velocity     = Vector2.zero;
        //     currientStateEnemy = StateEnemys.movingUp;
        //    
        // }
       
        Debug.Log("поиск плеера");
        // if(_moveX) {
        //     FindPlayerX();
        // }
        //
        // if(_moveY) {
        //     
        // FindPlayerY();
        // }
        
        FindPlayerX();
        FindPlayerY();
        switch(currientStateEnemy) {
            case StateEnemys.movingLeft :
                Debug.Log("иду в лево");
                MoveLeft();
                break;
            case StateEnemys.movingRight :
                MoveRight();
                Debug.Log("иду в право");
                break;
            case StateEnemys.movingUp :
                Debug.Log("иду в вверх");
                ClimbUp();
                break;
            case StateEnemys.movingDown :
                Debug.Log("иду в вниз");
                ClimbDown();
                break;
        }
       
    }

    public void MoveLeft() {
        currentDirection = -1;
        Vector3 localScale = new Vector3(currentDirection * 1f, 1f, 1f);
        graphics.localScale = localScale;
        Move();
        // IsProtectedFromEnvironment = true;
    }

    public void MoveRight() {
        currentDirection = 1;
        Vector3 localScale = new Vector3(currentDirection * 1f, 1f, 1f);
        graphics.localScale = localScale;
        Move();
        // IsProtectedFromEnvironment = true;
    }

    private void Move() {
        rb.velocity = new Vector2(SPEED * currentDirection, rb.velocity.y);
    }

    
    
    private void FindPlayerX() {
        var playerPos = playerGameObject.transform.position;
        if(playerPos.x > transform.position.x + 2f && _moveX) {
            Debug.Log("Player находится справа");

            currientStateEnemy = StateEnemys.movingRight;
        }

        if(playerPos.x < transform.position.x - 2f && _moveX) {
            Debug.Log("Player находится слева");
            currientStateEnemy = StateEnemys.movingLeft;
        }
    }
    
    private void FindPlayerY() {
        
        var playerPos = playerGameObject.transform.position;
        if(playerPos.y > transform.position.y + 1 && _moveY) {
            Debug.Log("Player находится выше");
            currientStateEnemy = StateEnemys.movingUp;
        }

        if(playerPos.y < transform.position.y - 1f && _moveY) {
            Debug.Log("Player находится ниже");
            currientStateEnemy = StateEnemys.movingDown;
            
        }
    }

    private void ClimbUp() {
        transform.Translate(SPEED * Time.deltaTime * Vector3.up);
    }

    private void ClimbDown() {
        transform.Translate(SPEED * Time.deltaTime * Vector3.down);
    }
    
    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.transform.CompareTag("ladder") && _moveX) {
            // isPossableLadderMove = true;
            // Debug.Log("какой колайдер словил энеми   =  " + other.gameObject.transform.name);
            _moveX = false;
            _moveY = true;
            Vector2 _ladderPos = other.gameObject.transform.position;
            transform.position = new Vector2(_ladderPos.x, transform.position.y);
            rb.gravityScale = 0f;
            rb.velocity = Vector2.zero;
            
        }
        
    }
    
    private void OnTriggerExit2D(Collider2D other) {
        rb.gravityScale = 1f;
        _moveX = true;
        _moveY = false;
        
    }
    
}