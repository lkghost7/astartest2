﻿using Pathfinding;
using UnityEngine;

public class SqurleAI : MonoBehaviour{
    public Transform targetPosition;

    private Seeker              seeker;
    private CharacterController controller;

    public Path path;

    public float speed = 2;

    public float nextWaypointDistance = 3;

    private int currentWaypoint = 0;

    public  float repathRate = 0.5f;
    private float lastRepath = float.NegativeInfinity;

    public bool reachedEndOfPath;

    private Rigidbody2D rb;

    public bool _moveXYOn;
    
    private bool _moveX = true;
    private bool _moveY;

    public void Start() {
        seeker     = GetComponent<Seeker>();
        controller = GetComponent<CharacterController>();
        rb         = GetComponent<Rigidbody2D>();
    }

    public void OnPathComplete(Path p) {
        // Debug.Log("error? " + p.error);

        p.Claim(this);
        if(!p.error) {
            if(path != null) path.Release(this);
            path            = p;
            currentWaypoint = 0;
        }
        else {
            p.Release(this);
        }
    }


    public void Update() {
        if(Time.time > lastRepath + repathRate && seeker.IsDone()) {
            lastRepath = Time.time;

            seeker.StartPath(transform.position, targetPosition.position, OnPathComplete);
        }

        if(path == null) {
            // We have no path to follow yet, so don't do anything
            return;
        }

        reachedEndOfPath = false;
        float distanceToWaypoint;
        while(true) {
            distanceToWaypoint = Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]);
            if(distanceToWaypoint < nextWaypointDistance) {
                if(currentWaypoint + 1 < path.vectorPath.Count) {
                    currentWaypoint++;
                }
                else {
                    reachedEndOfPath = true;
                    break;
                }
            }
            else {
                break;
            }
        }

        // var speedFactor = reachedEndOfPath ? Mathf.Sqrt(distanceToWaypoint/nextWaypointDistance) : 1f;
        Vector2 dir      = (path.vectorPath[currentWaypoint] - transform.position).normalized;
        Vector2 enemyPos = transform.position;
        // Vector3 dir = (path.vectorPath[currentWaypoint] - transform.position);

        // foreach(Vector3 vector3 in path.vectorPath) {
        //     print(vector3);
        // }

        Vector2 pointNode = path.vectorPath[currentWaypoint];
        // Debug.Log("что в диркешин на данный момент ___" + pointNode);
        // Debug.Log("что в трансформ на данный момент___" + enemyPos);


        if(_moveY) {
            print("Иду вверх");
            if(pointNode.y < enemyPos.y) {
                // rb.gravityScale = 0f;
                rb.velocity     = Vector2.down;
            }

            if(pointNode.y > enemyPos.y) {
                // rb.gravityScale = 0f;
                rb.velocity     = Vector2.up;
            }
        }

        if(_moveX) {
            print("иду вперед ");
            
        if(pointNode.x < enemyPos.x) {
            rb.gravityScale = 1f;
            rb.velocity     = Vector2.left * 1f;
        }
        
        if(pointNode.x > enemyPos.x) {
            rb.gravityScale = 1f;
            rb.velocity     = Vector2.right * 1f;
        }
        
        }

        // Debug.Log("какой трансформ   " +  transform.position);
        // Debug.Log("какой дирекшин   " + dir);

        // Vector3 velocity = dir * speed * Time.deltaTime;
        // Vector3 velocity = dir * speed;
        // controller.SimpleMove(velocity);

        // Debug.Log("нарпавление  Y___ " + dir.y);
        // Debug.Log("нарпавление X ___ " + dir.x);
        // transform.Translate(Vector3.left * Time.deltaTime);
    }
    
    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.transform.CompareTag("ladder") && _moveX) {
            // isPossableLadderMove = true;
            Debug.Log("какой колайдер словил энеми   =  " + other.gameObject.transform.name);
            _moveX = false;
            _moveY = true;
            Vector2 _ladderPos = other.gameObject.transform.position;
            transform.position = new Vector2(_ladderPos.x, transform.position.y);
            rb.gravityScale    = 0f;
            rb.velocity        = Vector2.zero;
            
        }
        
    }
    
    private void OnTriggerExit2D(Collider2D other) {
        rb.gravityScale = 1f;
        _moveX          = true;
        _moveY          = false;
        
    }
}
