﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ladders : MonoBehaviour{
    
    private EnemysController _enemysController;
    private Vector3 _ladder;
    
  
    void Start() {
        _enemysController = FindObjectOfType<EnemysController>();
        _ladder = transform.position;

    }

    void Update() {
        
    }

    private void OnTriggerEnter2D(Collider2D other) {
    
        _enemysController.isPossableLadderMove = true;
       
        other.gameObject.transform.position = new Vector3(_ladder.x,
                                                          other.gameObject.transform.position.y,
                                                          other.gameObject.transform.position.z);
    }
    
    private void OnTriggerExit2D(Collider2D other) {
       
        _enemysController.isPossableLadderMove = false;
    }

   
}