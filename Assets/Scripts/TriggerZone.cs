﻿using UnityEngine;

public abstract class TriggerZone : MonoBehaviour
{
    protected abstract void OnTriggerZoneEnter(ITriggerHandler handler);
    protected abstract void OnTriggerZoneExit(ITriggerHandler  handler);
}
