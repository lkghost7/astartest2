﻿using UnityEngine;

public enum StateLadder{
    movingUp,
    movingDown,
}

public class Ladder : TriggerZone{
    private       ITriggerHandler currentHandler;
    private       StateLadder     stateLadder;
    private       bool            isEnemyClimb;
    private       EnemyController enemyController;
    private const float           SPEED = 5f;

    private void Start() {
        enemyController = FindObjectOfType<EnemyController>();
    }

    private void Update() {
        if(currentHandler == null || currentHandler.IsProtectedFromEnvironment) {
            return;
        }

        var handlerPosition = ((MonoBehaviour) currentHandler).transform.position;
        var ladderPosition  = transform.position;

        // Центрируем по оси Х
        ((MonoBehaviour) currentHandler).transform.position =
            new Vector3(ladderPosition.x, handlerPosition.y, handlerPosition.z);
        switch(stateLadder) {
            case StateLadder.movingUp :
                // Debug.Log("Состояние = вверх");
                // Ползем вверх
                ClimbUp();
                break;
            case StateLadder.movingDown :
                // Debug.Log("Состояние = вниз");
                ((MonoBehaviour) currentHandler).transform.Translate(SPEED * Time.deltaTime * Vector3.down);
                break;
        }
    }

    // Триггер ловит любого у кого есть интерфейс ITriggerHandler
    protected override void OnTriggerZoneEnter(ITriggerHandler handler) {
        currentHandler = handler;
        Debug.Log("триггер интерфеса вошел");
        currentHandler.Activate(false);
    }

    protected override void OnTriggerZoneExit(ITriggerHandler handler) {
        currentHandler?.Activate(true);
        Debug.Log("триггер интерфеса вышел +++");
        currentHandler = null;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        Debug.Log("словил колайдер лестницы");
        if(!enemyController.IsProtectedFromEnvironment) {
            var handler = other.gameObject.GetComponent<ITriggerHandler>();
            if(handler != null) {
                OnTriggerZoneEnter(handler);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if(!enemyController.IsProtectedFromEnvironment) {
            var handler = other.gameObject.GetComponent<ITriggerHandler>();
            if(handler != null) {
                OnTriggerZoneExit(handler);
            }
        }
    }

    private void ClimbUp() {
        ((MonoBehaviour) currentHandler).transform.Translate(SPEED * Time.deltaTime * Vector3.up);
    }
    
    private void ClimbDown() {
        ((MonoBehaviour) currentHandler).transform.Translate(SPEED * Time.deltaTime * Vector3.up);
    }
}