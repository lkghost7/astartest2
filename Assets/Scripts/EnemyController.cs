﻿using UnityEngine;

public enum StateEnemy{
    movingLeft,
    movingRight,
    needUP,
    needDown
}

public class EnemyController : MonoBehaviour, ITriggerHandler{
    [SerializeField] private bool       left;
    [SerializeField] private bool       right;
    [SerializeField] private Transform  graphics;
    [SerializeField] private GameObject playerGameObject;

    private       StateEnemy  currientStateEnemy;
    private       Rigidbody2D rb;
    public        bool        IsProtectedFromEnvironment {get; set;}
    
    public        bool        IsMovingNow                {get; set;}
    private const float       SPEED            = 2f;
    private       int         currentDirection = 1;

    private void Start() {
        rb          = GetComponent<Rigidbody2D>();
        IsMovingNow = true;
        IsProtectedFromEnvironment = true;
        // IsProtectedFromEnvironment = false;
    }

    public void Update() {
        if(!IsMovingNow) {
            return;
        }
        Debug.Log("чему равна_ протектед___ " + IsProtectedFromEnvironment);

        FindPlayerY();
        FindPlayerX();
       
        switch(currientStateEnemy) {
            case StateEnemy.movingLeft :
                Debug.Log("иду в лево");
                MoveLeft();
                break;
            case StateEnemy.movingRight :
                MoveRight();
                Debug.Log("иду в право");
                break;
            case StateEnemy.needUP :
                Debug.Log("хочу на верх");
                IsProtectedFromEnvironment = false;
                break;
            case StateEnemy.needDown :
                Debug.Log("хочу вниз");
                // IsProtectedFromEnvironment = false;
                break;
        }
    }

    public void MoveLeft() {
        currentDirection = -1;
        Vector3 localScale = new Vector3(currentDirection * 1f, 1f, 1f);
        graphics.localScale = localScale;
        Move();
        // IsProtectedFromEnvironment = true;
    }
    public void MoveRight() {
        currentDirection = 1;
        Vector3 localScale = new Vector3(currentDirection * 1f, 1f, 1f);
        graphics.localScale = localScale;
        Move();
        // IsProtectedFromEnvironment = true;
    }

    public void Activate(bool isActive) {
        IsMovingNow = isActive;
        if(!isActive && rb != null) {
            rb.gravityScale = 0f;
            rb.velocity     = Vector2.zero;
        }
        else {
            if(rb != null) {
                rb.gravityScale = 3f;
            }
        }
    }

    private void Move() {
        rb.velocity = new Vector2(SPEED * currentDirection, rb.velocity.y);
    }

    private void FindPlayerY() {
        var playerPos = playerGameObject.transform.position;
        if(playerPos.y > transform.position.y + 3f) {
            // Debug.Log("Player находится выше");
            currientStateEnemy = StateEnemy.needUP;
        }

        if(playerPos.y < transform.position.y - 3f) {
            // Debug.Log("Player находится ниже");
            currientStateEnemy = StateEnemy.needDown;
            
        }
    }
    
    private bool FindPlayerX() {
        var playerPos = playerGameObject.transform.position;
        if(playerPos.x > transform.position.x + 3f) {
            // Debug.Log("Player находится справа");
           
            currientStateEnemy = StateEnemy.movingRight;
            return true;
        }

        if(playerPos.x < transform.position.x - 3f) {
            // Debug.Log("Player находится слева");
            currientStateEnemy = StateEnemy.movingLeft;
            return false;
        }

        return false;
    }
}